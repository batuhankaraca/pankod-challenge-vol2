

import React from 'react';
import Movies from './pages/Movies';
import Home from './pages/Home';
import Navbar from './components/Navbar';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

class App extends React.Component {

  render() {
    return (
      <Router>
        <div className="App">
          <Navbar/>
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/Movies" component={Movies} />
          </Switch>
        </div>
      </Router>
    );
  }

}

export default App;