import React from 'react';

function FetchError() {
    return (
        <div className="container section_container">
            <div className="row">
                <div className="col-sm-12 text-center">
                    <h2>Yüklenirken Bir Hata Oluştu...</h2>
                </div>
            </div>
        </div>
    );
}

export default FetchError;