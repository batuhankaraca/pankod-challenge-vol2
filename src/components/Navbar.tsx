import React from 'react';
import {Link} from 'react-router-dom';


function Navbar() {
    return (
        <div className="header">
            <ul>
                <li className="logo"><Link to="/"><h1>DEMO Streaming</h1></Link></li>
                <li><Link to="/Movies">Login</Link></li>
                <li><Link to="/" className="freetrail">Start Free Trail</Link></li>
            </ul>
        </div>
    );
}

export default Navbar;