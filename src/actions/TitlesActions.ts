import { ActionCreator, Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { ITitles, ITitlesState } from '../reducers/titleReducers';

export enum TitlesActionTypes {
  GET_ALL = 'GET_ALL',
}

export interface ITitlesGetAllAction {
  type: TitlesActionTypes.GET_ALL;
  titles: ITitles[];
}

export type TitlesActions = ITitlesGetAllAction;

export const getAllTitles: ActionCreator<
  ThunkAction<Promise<any>, ITitlesState, null, ITitlesGetAllAction>
> = () => {
  return async (dispatch: Dispatch) => {
    try {
      fetch("https://raw.githubusercontent.com/pankod/frontend-challenge/master/feed/sample.json")
        .then(response => response.json())
        .then((result) => {
          /* result.entries = result.entries.filter((res: { programType: string; }) => {
             return res.programType.match('movie');
           });*/
          result.entries = result.entries.filter((res: any) => {
            return res.releaseYear > 2010;
          });
          /*result.entries = result.entries.sort(function (a: any, b: any) {
            if (a.title < b.title) { return -1; }
            if (a.title > b.title) { return 1; }
            return 0;
          })*/
          //result.entries = result.entries.slice(0, 21);
          console.log(result);

          dispatch({
            titles: result.entries,
            type: TitlesActionTypes.GET_ALL,
          });
        },
        )



    } catch (err) {
      console.error(err);
    }
  };
};