/* eslint-disable jsx-a11y/alt-text */

import * as React from 'react';
import { connect } from 'react-redux';
import { IAppState } from '../store/Store';
import { ITitles, titlesReducer } from '../reducers/titleReducers';
import FetchError from '../components/FetchError';

interface IProps {
  titles: ITitles[];
}

class MoviesList extends React.Component<IProps, any> {

  constructor(props: IProps) {
    super(props);
    this.state = {
      posts: [],
    };
  }


   async componentDidMount(){
    /*this.setState({
      posts: this.props.titles.filter((res: { programType: string; }) => {
        return res.programType.match('movie');
      })
    });
    this.setState({
      posts: this.props.titles.sort(function (a: any, b: any) {
        if (a.title < b.title) { return -1; }
        if (a.title > b.title) { return 1; }
        return 0;
      })
    });
    this.setState({
      posts: this.props.titles.slice(0,21)
    });
    console.log(this.state.posts);*/
        
  }

  SearchTitle = (e : React.ChangeEvent<HTMLInputElement>) => {
    console.log(e.target.value);
    this.setState({
      posts: this.state.posts.filter((item: { title: { indexOf: (arg0: any) => number; }; }) => item.title.indexOf(e.target.value) !== -1)
    });
    console.log(this.state.posts);
    
  }
  public render() {
    var { posts } = this.state;
    var { titles } = this.props;
    
    titles = titles.filter((res: { programType: string; }) => {
      return res.programType.match('movie');
    });
    titles = titles.slice(0, 21);
    titles = titles.sort(function (a: any, b: any) {
      if (a.title < b.title) { return -1; }
      if (a.title > b.title) { return 1; }
      return 0;
    })
    posts = titles;


    if (posts.length === 0) {
      return <FetchError />
    }
    return (
      <>
        <input type="text" onChange={this.SearchTitle} />
        {posts &&
          posts.map((title: { title: string; images: { [x: string]: { url: string | undefined; }; }; releaseYear: React.ReactNode; }) => {
            return (
              <div className="col-sm-3 movies_list" key={title.title}>
                <img src={title.images['Poster Art'].url} />
                <div className="title_div">
                  <p>{title.title} <br /> {title.releaseYear}</p>
                </div>
              </div>
            );
          })}
      </>
    );
  }
}

const mapStateToProps = (store: IAppState) => {
  return {
    titles: store.titlesState.titles,
  };
};

export default connect(mapStateToProps)(MoviesList);
