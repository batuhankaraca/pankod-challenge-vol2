import { Reducer } from 'redux';
import {
  TitlesActions,
  TitlesActionTypes,
} from '../actions/TitlesActions';

export interface ITitles {
  title: string;
  description: string;
  programType: string;
  releaseYear: string;
  [images: string]: any;
}

export interface ITitlesState {
  titles: ITitles[];
}

const initialTitlesState: ITitlesState = {
  titles: [],
};

export const titlesReducer: Reducer<ITitlesState, TitlesActions> = (
  state = initialTitlesState,
  action: { type: string; titles: any; }
) => {
  switch (action.type) {
    case TitlesActionTypes.GET_ALL: {
      return {
        ...state,
        titles: action.titles,
      };
    }
    default:
      return state;
  }
};