
import { applyMiddleware, combineReducers, createStore, Store } from 'redux';
import thunk from 'redux-thunk';

import {
  titlesReducer,
  ITitlesState,
} from '../reducers/titleReducers';

export interface IAppState {
  titlesState: ITitlesState;
}

const rootReducer = combineReducers<IAppState>({
  titlesState: titlesReducer,
});

export default function configureStore(): Store<IAppState, any> {
  const store = createStore(rootReducer, undefined, applyMiddleware(thunk));  
  return store;
}