import * as React from 'react';
import '../App.css';

import MoviesList from '../containers/MoviesList';

const Movies: React.SFC<{}> = () => {
  return (
    <>
      <div className="container section_container">
        <div className="row">
          <div className="col-4">
            
          </div>
        </div>
        <div className="row">
          <MoviesList />
        </div>

      </div>
    </>
  );
};

export default Movies;
